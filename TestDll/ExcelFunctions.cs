﻿using System;
using System.Runtime.InteropServices;
using RGiesecke.DllExport;
using Excel = Microsoft.Office.Interop.Excel;
using NExcel = NetOffice.ExcelApi;

namespace TestDll
{
    public class ExcelFunctions
    {
        [DllExport(nameof(DoSomethingWithSheet), CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static bool DoSomethingWithSheet(object sheetObject)
        {
            try
            {
                if (!(sheetObject is Excel.Worksheet))
                    throw new ArgumentException($"{nameof(sheetObject)} is not of type {nameof(Excel.Worksheet)}");

                var sheet = (Excel.Worksheet) sheetObject;

                General.Output($"Sheet name: {sheet.Name}{Environment.NewLine}");

                return true;
            }
            catch (Exception ex)
            {
                General.Output(ex.ToString());
                return false;
            }
            finally
            {
                while (Marshal.ReleaseComObject(sheetObject) > 0) ;
            }

        }

        [DllExport(nameof(DoSomethingWithSheetNetOffice), CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static bool DoSomethingWithSheetNetOffice(object sheetObject)
        {
            try
            {
                using (var sheet = new NExcel.Worksheet(null, sheetObject))
                {
                    sheet.Name = sheet.Name.TrimEnd("1234567890".ToCharArray()) + 77;
                }

                return true;
            }
            catch (Exception ex)
            {
                General.Output(ex.ToString());
                return false;
            }
            finally
            {
                while (Marshal.ReleaseComObject(sheetObject) > 0);
            }
        }
    }
}
