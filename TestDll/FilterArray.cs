﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using CSScriptLibrary;
using RGiesecke.DllExport;

namespace TestDll
{
    public class FilterArray
    {
        [DllExport(nameof(FilterStringArray), CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static bool FilterStringArray(
            ref object array,
            [MarshalAs(UnmanagedType.AnsiBStr)] string lambdaExpression)
        {
            try
            {

                if (array.GetType() != typeof(string[]))
                {
                    array = new[] {$"Parameter {nameof(array)} is not a string array"};
                    return false;
                }

                lambdaExpression =
                    "using System;" + Environment.NewLine 
                    + "using System.Collections.Generic;" + Environment.NewLine
                    + "using System.Linq;" + Environment.NewLine
                    + "namespace TempNameSpace {" + Environment.NewLine
                    + "    public class TempClass {" + Environment.NewLine
                    + "        public static Func<string, bool> GetPredicate() { return " + lambdaExpression + "; }" + Environment.NewLine
                    + "    }" + Environment.NewLine
                    + "}" + Environment.NewLine;

                General.Output(lambdaExpression);

                var script = new AsmHelper(CSScript.LoadCode(lambdaExpression, "System.Xml.Linq"));

                array = ((string[]) array).Where((Func<string, bool>) script.Invoke("*.GetPredicate")).ToArray();

                return true;
            }
            catch (Exception ex)
            {
                array = new[] {"Exception: " + ex};
                return false;
            }
        }
    }
}