﻿using System;
using System.Runtime.InteropServices;

namespace ImportTest
{
    class Program
    {
        private const string _dllPath = @"C:\Users\johanb\Source\Repos\TestDll\TestDll\bin\Debug\TestDll.dll";

        [DllImport(_dllPath, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetFilesByExtensions(
            ref object arrayOfFiles,
            [MarshalAs(UnmanagedType.AnsiBStr)] string folderPath,
            object extensions,
            [MarshalAs(UnmanagedType.Bool)] bool includeSubdirectories = true);


        [DllImport(_dllPath, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool FilterStringArray(
            ref object array,
            [MarshalAs(UnmanagedType.AnsiBStr)] string lambdaExpression);


        static void Main()
        {
            try
            {
                object myarr = new[] { "JohaN", "johan", "JOHAN", "JOHAN JOHAN" };

                FilterStringArray(ref myarr, "s => s.ToCharArray().All(c => !(char.IsLower(c)))");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.ReadLine();
        }

        private static void OldTest()
        {
            object result = null;

            if (GetFilesByExtensions(ref result, @"C:\Tradostest\Project 4", new[] { ".sdlxliff" }))
            {
                var files = (string[])result;

                foreach (var file in files)
                {
                    Console.WriteLine(" - " + file);
                }
            }
            else
            {
                Console.WriteLine("Error: " + (result == null ? "unknown error" : (result as string[])[0]));
            }
        }
    }
}
