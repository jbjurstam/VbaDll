﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using RGiesecke.DllExport;
using System.IO.Compression;
using System.Xml.Linq;

namespace DocxProperties
{
    public class DocxPropertyGetter
    {
        [DllExport(nameof(GetDocxProp), CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.AnsiBStr)]
        public static string GetDocxProp([MarshalAs(UnmanagedType.AnsiBStr)] string wordPath, [MarshalAs(UnmanagedType.AnsiBStr)] string propName)
        {
            try
            {
                using (var fileStream = File.Open(wordPath, FileMode.Open))
                using (var parentArchive = new ZipArchive(fileStream))
                {
                    return GetPropName(parentArchive, propName);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private static string GetPropName(ZipArchive parentArchive, string propName)
        {
            var core = parentArchive.Entries.FirstOrDefault(e => e.FullName == "docProps/core.xml");

            using (var coreStream = core.Open())
            {
                var doc = XDocument.Load(coreStream);

                foreach (var descendant in doc.Descendants())
                {
                    if (descendant.Name.LocalName.Equals(propName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return descendant.Value;
                    }
                }
            }

            return string.Empty;
        }

    }
}