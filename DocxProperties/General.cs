﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DocxProperties
{
    static class General
    {
        public static void Output(string str)
        {
            File.AppendAllText(LogPath, str);
        }

        public static void NormalizeANSI(ref string[] files)
        {
            for (int i = 0; i < files.Length; i++)
            {
                files[i] = string.Concat(files[i].Normalize(NormalizationForm.FormD).Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark));
            }
        }

        public static string LogPath
        {
            get
            {
                return Path.Combine(AssemblyPath + ".log");
            }
        }

        public static void OutputLine(string v)
        {
            Output(v + Environment.NewLine);
        }

        private static string _assemblyPath;

        public static string AssemblyPath
        {
            get
            {
                if (_assemblyPath == null)
                {
                    var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                    var uri = new UriBuilder(codeBase);
                    var path = Uri.UnescapeDataString(uri.Path);
                    _assemblyPath = path;
                }

                return _assemblyPath;
            }
        }
    }
}