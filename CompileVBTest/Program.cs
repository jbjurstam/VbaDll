﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.CSharp;

namespace StringCompilation
{
    class Program
    {
        private static Evaluator<CSharpCodeProvider> _cSharp = new Evaluator<CSharpCodeProvider>();
        private static Evaluator<VBCodeProvider> _vb = new Evaluator<VBCodeProvider>();

        static void Main()
        {
            try
            {
                TestEval();
                TestDelegates();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.ReadLine();
        }

        private static void TestDelegates()
        {
            var stringDelegate = _cSharp.CompileLambda<Func<string, bool>>("s => s.ToCharArray().All(c => !(char.IsLower(c)))");

            foreach (var str in new[] { "JohaN", "johan", "JOHAN", "JOHAN JOHAN" }.Where(stringDelegate))
            {
                Console.WriteLine(str);
            }


            //var vbFunc = @"Function(i As Integer)
            //    Return i > 8
            //End Function";


            var intDelegate = _vb.CompileDelegate<Func<int, bool>>("Function(i As Integer) i > 8");
            foreach (var @int in new[] { 10, 9, 8, 11, 1 }.Where(intDelegate))
            {
                Console.WriteLine(@int);
            }


            var intDelegate2 = _cSharp.CompileLambda<Func<int, bool>>("i => i > 8");
            foreach (var @int in new[] { 10, 9, 8, 11, 1 }.Where(intDelegate2))
            {
                Console.WriteLine(@int);
            }

            var intDelegate3 = _cSharp.CompileDelegate<Func<int, bool>>("delegate (int i) { return i > 8; }");
            foreach (var @int in new[] { 10, 9, 8, 11, 1 }.Where(intDelegate3))
            {
                Console.WriteLine(@int);
            }
        }

        private static void TestEval()
        {
            Console.WriteLine(
                                _vb.Eval(
                                    vbCode: "new StringBuilder(\"hahah\")",
                                    imports: new[] { "System.Text" }
                                )
                            );

            Console.WriteLine(
                _cSharp.Eval(
                    vbCode: "new StringBuilder(\"hahah\")",
                    imports: new[] { "System.Text" }
                )
            );

            Console.WriteLine(
                string.Join(Environment.NewLine,
                    _vb.Eval<List<string>>(
                        vbCode: "New List(Of String)(New String() {\"one\", \"two\", \"three\"})",
                        imports: new[] { "System.Collections", "System.Collections.Generic" }
                    )
                )
            );
        }
    }
}